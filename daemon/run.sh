#!/bin/bash

# init message 
echo "Creating virtual port on background."

# virtual port
( socat pty,link=/dev/ttyV0,waitslave tcp:10.11.12.155:1001) &

# sleep
sleep 5

# init message 
echo "Run GW daemon."

# daemon
/usr/bin/iqrfgd2 /etc/iqrf-gateway-daemon/config.json
